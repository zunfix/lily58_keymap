#include QMK_KEYBOARD_H

enum custom_macros {
    HP_0 = SAFE_RANGE, // Harpoon 0
    HP_1,              // Harpoon 1
    HP_2,              // Harpoon 2
    HP_3,              // Harpoon 3
    TMP_1,             // Tmux pane 1
    TMP_2,             // Tmux pane 2
    TMP_3,             // Tmux pane 3
    TMP_4,             // Tmux pane 4
    TMW_NEXT,          // Tmux window next
    TMW_PREV,          // Tmux window prev
    HYPR_JUMP_1,
    HYPR_JUMP_2,
    HYPR_JUMP_3,
    HYPR_JUMP_4,
    HYPR_SEND_1,
    HYPR_SEND_2,
    HYPR_SEND_3,
    HYPR_SEND_4,
    TMX_C,
    TMX_SHIFT_C,
};

enum layer_number {
    LAYER_0 = 0,
    LAYER_1,
    LAYER_2,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* QWERTY
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | ESC  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  `   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |                    |   Y  |   U  |   I  |   O  |   P  |  -   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |LCTRL |   A  |   S  |   D  |   F  |   G  |-------.    ,-------|   H  |   J  |   K  |   L  |   ;  |  '   |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |LShift|   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   N  |   M  |   ,  |   .  |   /  |RShift|
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */

    [LAYER_0] = LAYOUT(
        TMX_C,   TMP_1,  TMP_2,   TMP_3,   TMP_4,   TMX_SHIFT_C,                         KC_NO,  HP_0,  HP_1,     HP_2,    HP_3,     KC_NO,
        KC_TAB,  KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                          KC_Y,   KC_U,  KC_I,     KC_O,    KC_P,     KC_BSPC,
        KC_ESC,  KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                          KC_H,   KC_J,  KC_K,     KC_L,    KC_SCLN,  KC_QUOT,
        KC_LSFT, KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, TMW_PREV,     TMW_NEXT,  KC_N,   KC_M,  KC_COMM,  KC_DOT,  KC_SLSH,  KC_RSFT,
                                KC_LCTL, KC_LGUI, MO(LAYER_1), KC_SPC,  KC_ENT,  MO(LAYER_2), KC_NO, KC_LALT
    ),
/* LOWER
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |                    |  F7  |  F8  |  F9  | F10  | F11  | F12  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   `  |   !  |   @  |   #  |   $  |   %  |-------.    ,-------|   ^  |   &  |   *  |   (  |   )  |   -  |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |   _  |   +  |   {  |   }  |   |  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
    [LAYER_1] = LAYOUT(
    KC_NO,  HYPR_JUMP_1,  HYPR_JUMP_2,  HYPR_JUMP_3,   HYPR_JUMP_4,   KC_NO,                    KC_NO,    KC_NO,    KC_NO,  KC_NO,     KC_NO,  KC_NO,
    KC_NO,  KC_1,   KC_2,   KC_3,    KC_4,    KC_5,                     KC_6,     KC_7,     KC_8,   KC_9,      KC_0,   KC_NO,
    KC_F1,  KC_F2,  KC_F3,  KC_F4,   KC_F5,   KC_F6,                    KC_LEFT,  KC_DOWN,  KC_UP,  KC_RIGHT,  KC_NO,  KC_NO,
    KC_F7,  KC_F8,  KC_F9,  KC_F10,  KC_F11,  KC_F12, KC_NO,    KC_NO,  KC_PSCR,  KC_DEL,   KC_NO,  KC_NO,     KC_NO,  KC_NO,
                                KC_NO, KC_NO, KC_TRNS, KC_NO,   KC_NO,  KC_NO, KC_NO, KC_NO
    ),
/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   `  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |-------.    ,-------|      | Left | Down |  Up  |Right |      |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |  F7  |  F8  |  F9  | F10  | F11  | F12  |-------|    |-------|   +  |   -  |   =  |   [  |   ]  |   \  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
    [LAYER_2] = LAYOUT(
    KC_NO,  HYPR_SEND_1,    HYPR_SEND_2,   HYPR_SEND_3,   HYPR_SEND_4,   KC_NO,                   KC_NO,    KC_NO,   KC_NO,    KC_NO,    KC_NO,    KC_NO,
    KC_NO,  KC_EXLM,  KC_AT,   KC_HASH,  KC_DLR,  KC_PERC,                 KC_CIRC,  KC_AMPR, KC_ASTR,  KC_LPRN,  KC_RPRN,  KC_PIPE,
    KC_NO,  KC_UNDS,  KC_LCBR, KC_RCBR,  KC_GRV,  KC_TILD,                 KC_PLUS,  KC_MINS, KC_EQL,   KC_LBRC,  KC_RBRC,  KC_BSLS,
    KC_NO,  KC_NO,    KC_NO,   KC_NO,    KC_NO,   KC_NO,   KC_NO,   KC_NO, KC_NO,    KC_NO,   KC_NO,    KC_NO,    KC_NO,    KC_NO,
                              KC_NO, KC_NO, KC_NO, KC_NO,  KC_NO,   KC_TRNS, KC_NO, KC_NO
    ),

};

// layer_state_t layer_state_set_user(layer_state_t state)
// {
//     return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
// }

bool process_record_user(uint16_t keycode, keyrecord_t *record)
{
    switch (keycode) {
        case HP_0:
            if (record->event.pressed) {
                SEND_STRING(" u");
            }
            return false;
        case HP_1:
            if (record->event.pressed) {
                SEND_STRING(" i");
            }
            return false;
        case HP_2:
            if (record->event.pressed) {
                SEND_STRING(" o");
            }
            return false;
        case HP_3:
            if (record->event.pressed) {
                SEND_STRING(" p");
            }
            return false;
        case TMP_1:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")"1");
            }
            return false;
        case TMP_2:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")"2");
            }
            return false;
        case TMP_3:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")"3");
            }
            return false;
        case TMP_4:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")"4");
            }
            return false;
        case TMW_NEXT:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")SS_LSFT("0"));
            }
            return false;
        case TMW_PREV:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")SS_LSFT("9"));
            }
            return false;
        case HYPR_JUMP_1:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("1"));
            }
            return false;
        case HYPR_JUMP_2:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("2"));
            }
            return false;
        case HYPR_JUMP_3:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("3"));
            }
            return false;
        case HYPR_JUMP_4:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("4"));
            }
            return false;
        case HYPR_SEND_1:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("!"));
            }
            return false;
        case HYPR_SEND_2:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("@"));
            }
            return false;
        case HYPR_SEND_3:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("#"));
            }
            return false;
        case HYPR_SEND_4:
            if (record->event.pressed) {
                SEND_STRING(SS_LGUI("$"));
            }
            return false;
        case TMX_C:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")"c");
            }
            return false;
        case TMX_SHIFT_C:
            if (record->event.pressed) {
                SEND_STRING(SS_LCTL("s")"C");
            }
            return false;
    }
    return true;
}
